public class ProviderOutput {

    private int providerId;
    private int regionId;
    private int numberPackages;

    public ProviderOutput(int providerId, int regionId, int numberPackages) {
        this.providerId = providerId;
        this.regionId = regionId;
        this.numberPackages = numberPackages;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getNumberPackages() {
        return numberPackages;
    }

    public void setNumberPackages(int numberPackages) {
        this.numberPackages = numberPackages;
    }
}
