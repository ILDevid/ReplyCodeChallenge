import java.util.ArrayList;
import java.util.List;

public class ProjectOutput {

    private List<ProviderOutput> providerOutputs;

    public ProjectOutput() {
        providerOutputs = new ArrayList<>();
    }

    public List<ProviderOutput> getProviderOutputs() {
        return providerOutputs;
    }

    public void setProviderOutputs(List<ProviderOutput> providerOutputs) {
        this.providerOutputs = providerOutputs;
    }

}
