import java.util.ArrayList;
import java.util.List;

public class Provider {

    private String name;
    private List<Region> regions;

    public Provider(String name) {
        regions = new ArrayList<>();
        this.name = name;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
