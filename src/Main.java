import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;


public class Main {
	private static final String path = "./first_adventure.in";
    private static final String pathSolution = "./first_adventure.out";
    
    
    private static List<Provider> providers = new ArrayList<>();
    private static String[] regions;
    private static String[] services;
    private static List<Project> projects = new ArrayList<>();


	private static int V;
	private static int S;
	private static int C;
	private static int P;


	public static void main(String[] args) {
		readData();
		List<Region> regions;
		Project project;
		Region region;
		Provider provider;
		int p = 0;
		boolean need_provider = true;
		for (int i = 0; i < projects.size(); i++) {
			project = projects.get(i);
			while(need_provider) {
				provider = providers.get(p);
				regions = provider.getRegions();
				int r = 0;
				while(need_provider) {
					region = regions.get(p);
					for (int j = 0; j < S; j++) {
						Service s = project.getServices()[j];
						if(s.getboughtUnits() < s.getinitialUnits())
						{
							if(region.getAvailablePackages() >= 0)
							{
								//compriamo 
								int toBuy = region.getAvailablePackages() * region.getServices()[j];
								s.setboughtUnits(toBuy);
								region.setAvailablePackages(0);
								//aggiornamento delle altre unita.
								for (int k = 0; k < S; k++) {
									if(k!= j)
									{
										Service service = project.getServices()[k];
										toBuy = region.getAvailablePackages() * region.getServices()[k];
										service.setboughtUnits(toBuy);
									}
								}
							}
						}
					}
				}
			}
		}
	}


	 private static void readData() {
	        try {
	            Scanner s = new Scanner(new File(path));
	            V = s.nextInt();
	            S = s.nextInt();
	            C = s.nextInt();
	            P = s.nextInt();
	            services = new String[S];
	            //read services
	            for(int c=0; c<S; c++) {
	                services[c] = s.next();
	            }
	            regions = new String[C];
	            //read regions
	            for(int v=0; v<C; v++) {
	                regions[v] = s.next();
	            }
	            for(int i = 0; i < V; i++) {
	                String name = s.next();
	                int R = s.nextInt();
	                Provider provider = new Provider(name);
	                for(int j=0; j<R; j++) {
	                    String regionName = s.next();
	                    int availablePackages = s.nextInt();
	                    float unitCost = Float.parseFloat(s.next());
	                    Region region = new Region(regionName);
	                    region.setAvailablePackages(availablePackages);
	                    region.setUnitCost(unitCost);
	                    int[] units = new int[S];
	                    for(int k=0; k<S; k++) {
	                        units[k] = s.nextInt();
	                    }
	                    region.setServices(units);
	                    for(int l=0; l<C; l++) {
	                        region.getLatencies().add(s.nextInt());
	                    }
	                    provider.getRegions().add(region);
	                }
	                providers.add(provider);
	            }
	            for(int i=0; i<P; i++) {
	                int penalty = s.nextInt();
	                String country = s.next();
	                Project project = new Project(country, penalty);
	                int[] units = new int[S];
	                for(int j=0; j<S; j++) {
	                    units[j] = s.nextInt();
	                }
	                for (int k = 0; k < units.length; k++) {
	                	project.getServices()[k].setinitialUnits(units[k]);
					}
	             
	                projects.add(project);
	            }
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        }
	    }

	    private static void writeData(Output output) {
	        try {
	            FileWriter writer = new FileWriter(new File(pathSolution));
	            for(int i=0; i<P; i++) {
	                ProjectOutput projectOutput = output.getProjects()[i];
	                for(ProviderOutput providerOutput : projectOutput.getProviderOutputs()) {
	                    writer.write(providerOutput.getProviderId() + " ");
	                    writer.write(providerOutput.getRegionId() + " ");
	                    writer.write(providerOutput.getNumberPackages() + " ");
	                }
	                writer.write("\n");
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    private float getScore(Output output) {

	        float totalScore = 0;

	        for(int i=0; i<output.getProjects().length; i++) {

	            float score = 0;
	            float totalCost = 0;

	            ProjectOutput projectOutput = output.getProjects()[i];
	            for (int j = 0; j < projectOutput.getProviderOutputs().size(); j++) {
	                ProviderOutput providerOutput = projectOutput.getProviderOutputs().get(j);
	                Provider provider = providers.get(providerOutput.getProviderId());
	                Region region = provider.getRegions().get(providerOutput.getRegionId());
	                float unitCost = region.getUnitCost();
	                float totalServiceCost = unitCost * providerOutput.getNumberPackages();
	                totalCost += totalServiceCost;
	            }
	            //average latency
	            float averageLatency = 0;
	            float numeratore = 0;
	            float denominatore = 0;
	            for (Project project : projects) {
	                String countryName = project.getCountry();
	                int indexRegion = 0;
	                for (int j = 0; j < regions.length; j++) {
	                    if (regions[j].equals(countryName)) {
	                        indexRegion = j;
	                        break;
	                    }
	                }

	                for (int j = 0; j < projectOutput.getProviderOutputs().size(); j++) {
	                    ProviderOutput providerOutput = projectOutput.getProviderOutputs().get(j);
	                    Provider provider = providers.get(providerOutput.getProviderId());
	                    Region region = provider.getRegions().get(providerOutput.getRegionId());
	                    int Lr = region.getLatencies().get(indexRegion);
	                    int Ur = providerOutput.getNumberPackages();
	                    numeratore += (Lr * Ur);
	                    denominatore += Ur;
	                }
	            }
	            averageLatency = numeratore / denominatore;

	            //availability index
	            numeratore = 0;
	            denominatore = 0;
	            for (int j = 0; j < projectOutput.getProviderOutputs().size(); j++) {
	                ProviderOutput providerOutput = projectOutput.getProviderOutputs().get(j);
	                numeratore += providerOutput.getNumberPackages();
	                denominatore += Math.pow(providerOutput.getNumberPackages(), 2);
	            }

	            numeratore = (float) Math.pow(numeratore, 2);
	            float availabilityIndex = numeratore / denominatore;

	            int requestedUnits = 0;
	            for (int j = 0; j < projectOutput.getProviderOutputs().size(); j++) {
	                ProviderOutput providerOutput = projectOutput.getProviderOutputs().get(j);
	                requestedUnits += providerOutput.getNumberPackages();
	            }

	            int neededUnits = 0;
	            Project project = projects.get(i);
	            for(int j=0; j<project.getServices().length; j++) {
	                neededUnits += project.getServices()[j].getinitialUnits();
	            }
	            float slaPenalty = ((neededUnits - requestedUnits) / neededUnits) * project.getPenalty();

	            score = (float) (Math.pow(10, 9) / (((totalCost * averageLatency) / availabilityIndex) + slaPenalty));

	            totalScore += score;

	        }

	        return totalScore;
	    }


}
