public class Output {

    private ProjectOutput[] projects;

    public Output(int V) {
        projects = new ProjectOutput[V];
    }

    public ProjectOutput[] getProjects() {
        return projects;
    }

    public void setProjects(ProjectOutput[] projects) {
        this.projects = projects;
    }

}
